//Debugging Variables
const appName = 'csvParser.controller';
const Mydebbuger = require('debug')(appName);
var multer  = require('multer');

//CSV parsing and file handling Elements
const csv = require('csv-parser')
const fs = require('fs')

var upload = multer({ dest: 'multeruploads/' });

//declare array to store multiple users
const usersArray = [];

const getCsvParser = (req, res) => {
  Mydebbuger('Calling getCsvParser');
  try {
    const myResponse = "Csv Parser API...!"
    res.status(404).send(myResponse);
  } catch (e) {
    Mydebbuger('error obtained in the getCsvParser: ', e);
  }
}


const uploadCsvFile = (req, res) => {
  Mydebbuger('Calling uploadCsvFile');
  let myResponse = "Upload CSV executed successfully";
  try {
	upload.single('file');	
	res.end("File uploaded.");
	Mydebbuger('File uploaded.');
  } catch (e) {
    Mydebbuger('error obtained in the uploadCsvFile: ', e);
  }
}

const getSearchResults = (req, res) => {
  Mydebbuger('Calling getSearchResults');
  try {
    const myResponse = "Getting Search results..."
    res.send(myResponse);
  } catch (e) {
    Mydebbuger('Error obtained in the getSearchResults: ', e);
  }
}

const postNotRouteValid = (req, res) => {
  Mydebbuger('Calling postNotRouteValid');
  try {
    const myResponse = "Invalid Route, does not Exist."
    res.send(myResponse);
  } catch (e) {
    Mydebbuger('Error obtained in the postNotRouteValid: ', e);
  }
}

const getNotRouteValid = (req, res) => {
  Mydebbuger('Calling getNotRouteValid');
  try {
    const myResponse = "Invalid Route, does not Exist."
    res.send(myResponse);
  } catch (e) {
    Mydebbuger('Error obtained in the getNotRouteValid: ', e);
  }
}

module.exports = {
  getCsvParser,
  getSearchResults,
  getNotRouteValid,
  uploadCsvFile,
  postNotRouteValid,
}
