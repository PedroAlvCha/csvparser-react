require('dotenv').config()
const appName = 'csvparser.api';
const express = require('express');
const socketIO = require('socket.io');
const http = require('http');
const bodyParser = require('body-parser');
const Mydebbuger = require('debug')(appName);
const Sentry = require('@sentry/node');
const port = process.env.CSVPARSER_API_PORT || process.env.SETUP_CVPARSER_API_PORT; //obtain the PORT for the service
const routes = require('./routes/routes');


Sentry.init({ dsn: process.env.sentry_csvparser_node });

const app = express();

Mydebbuger('Attempting to create server instance with http');
const httpServer = http.createServer(app);

Mydebbuger('Attempting to create socket instance with server.');
const myIO = socketIO(httpServer);

Mydebbuger('Listening to the socket.');
myIO.on('connection', mySocket => {
  Mydebbuger('User connected');
  
  mySocket.on('holi', (greeting) => {
    Mydebbuger('Holi was emmitted');
	Mydebbuger('The greeting received was ', greeting);
    myIO.sockets.emit('Greeting', greeting)
  })
  
  mySocket.on('disconnect', () => {
    Mydebbuger('user disconnected');
  })
})

Mydebbuger('Attempting to start server listening on port:', port);
// start the server
httpServer.listen(port, (err) => {
  if(err) {
    Mydebbuger('Error at server creation: ', err);
    process.exit(1);
  } else {
    Mydebbuger('csvparser-api server listening port: ', port);
  }
});

Mydebbuger('Adding middlewares to our server');

try{
  // midlewares
  app.use((req, res, next) => {
    Mydebbuger('App is in use, got called with this URL: ', req.url, ' and request method', req.method);
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Request-Method', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
} catch (e2) {
  Mydebbuger('Error adding middlewares for server creations: ', e2);
}
// routes
try {
  Mydebbuger('We are now assigning route handling.');
  routes(app);
} catch (e) {
  Mydebbuger('Error in route assignment for server creations: ', e);
}

module.exports = httpServer;
