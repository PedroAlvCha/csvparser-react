import socketIOClient from 'socket.io-client'
import * as Sentry from '@sentry/browser';

const apiEndpoint = process.env.CSVPARSER_API_URL || 'http://localhost:9001';
const mySocket = socketIOClient(apiEndpoint);

Sentry.init({
 dsn: "https://e354b6be442b42c88284daa1a60017cf@sentry.io/1361010"
});

export function emitGreetingSocket (greeting){
  console.log('emitGreetingSocket got called');
  //console.log('The value for greeting is:', greeting);
  try{
	mySocket.emit('holi', 'hola');
  } catch (e) {
	  let myErrorE = e;
	  myErrorE.position = 'emitGreetingSocket';
	  console.log('We have an error in emitGreetingSocket:', myErrorE);
	  Sentry.captureException(myErrorE);
  }
}

export const getSearchResults = (query) =>
    fetch(`${apiEndpoint}/search`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: JSON.stringify(query)
    }).then(res => res.json())
      .then(data => data)

export const uploadCsvFile = (csvFile) =>
    fetch(`${apiEndpoint}/import`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: JSON.stringify(csvFile),
      }).then(res => res.json())
        .then(data => data)
