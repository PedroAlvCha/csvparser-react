import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import * as Sentry from '@sentry/browser';
import {  Button } from 'react-bootstrap';
import FileUploadProgress  from 'react-fileupload-progress';
const apiEndpoint = 'http://localhost:9001/import';

class FileUploadComponent extends Component {
  state = {
    categoryList: [],
    isNewPostModalOpen: 0,
  }

  componentDidCatch(error, errorInfo) {
    Sentry.withScope(scope => {
      Object.keys(errorInfo).forEach(key => {
        scope.setExtra(key, errorInfo[key]);
      });
	  console.log('We caught an error at UserSearch:', error);
      Sentry.captureException(error);
    });
  }

  render(){
    const {  myLocalError} = this.props;
    if(myLocalError){
      return (
        <Button onClick={() => Sentry.showReportDialog()}>Report feedback</Button>
      );
	} else {
	  return(
          <div>
			<FileUploadProgress key='ex1' url={apiEndpoint}
			  onProgress={(e, request, progress) => {console.log('progress', e, request, progress);}}
			  onLoad={ (e, request) => {console.log('load', e, request);}}
			  onError={ (e, request) => {console.log('error', e, request);}}
			  onAbort={ (e, request) => {console.log('abort', e, request);}}
			  />
          </div>
      )
	}
  }
};

function mapStateToProps (state, ownProps) {
  return {
	  myLocalError:state.csvParserManager.error,
  }
};

function mapDispatchToProps (dispatch) {
  return {
  }
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FileUploadComponent);
