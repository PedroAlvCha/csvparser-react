# csvparser-react
This is a csvparser written in React.js as a proof of concept. It is not intended to be a full application.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This project is designed to be run on Docker for ease of testing and deployment. Please make sure that Docker is installed on your local machine or where you plan to run this app. You can [get Docker for free here](https://hub.docker.com/search/?type=edition&offering=community).

### Installing Locally

To use this App, you must first clone the repository csvparser-react.

```bash
$ git clone https://github.com/PedroAlvCha/csvparser-react.git
```

Once you have the repo cloned, move to the project folder and install the app. 

```bash
$ cd csvparser-react
$ npm install
```

Afterwards, install the api server. 

```bash
$ cd apiserver
$ npm install
```

The apiserver contains a file called .envPattern. This file must be copied and renamed ".env".

Finish by installing the client. 

```bash
$ cd ..
$ cd client
$ npm install
```


## Usage

### Linux

If you are on a linux machine you can start both the apiserver and the client from the root folder of the project by using the command:

```bash
$ npm run dev
```

If you only want to run the apiserver then use the command:

```bash
$ npm run server
```

If you only want to run the client then use the command:

```bash
$ npm run client
```

### Windows

If you are on a Windows machine you need to start the client and the server separately. For starting the apiserver use the command:

```bash
$ cd apiserver
$ npm run startapi
```

Open a separate command prompt interface and run the client: 

```bash
$ cd apiserver
$ npm run start
```

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [React](https://reactjs.org/) - The API backend used
* [Express](https://expressjs.com) - The Web Server used
* [npm package.json](https://docs.npmjs.com/creating-a-package-json-file) - Dependency Management


## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/PedroAlvCha/csvparser-react/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

[SemVer](http://semver.org/) was used for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/PedroAlvCha/csvparser-react/tags). 

## Author

* **Pedro Alvarado Chavarria**  [gitlab](https://gitlab.com/PedroAlvCha), [linkedin](https://www.linkedin.com/in/pedroalvaradochavarria/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* README.md format obtained from https://gist.github.com/PurpleBooth/109311bb0361f32d87a2


